<?php get_template_part('templates/head'); ?>
<body <?php body_class(); ?>>

   <?php /* Google Tag Manager */ ?>
   <?php if( get_field('google_tag_manager_body', 'option') ): ?>
      <?php the_field('google_tag_manager_body', 'option'); ?>
   <?php endif; ?>  

    <!--[if lt IE 8]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'roots'); ?>
      </div>
    <![endif]-->

  <?php
    do_action('get_header');
    // Choose the correct header
    // pick which one in config.php
    if (current_theme_supports('header-two-navs')) {
      get_template_part( 'templates/header-two-navs' );
    } elseif (current_theme_supports('header-nav-bottom')) {
      get_template_part( 'templates/header-nav-bottom' );
    } else {
      get_template_part( 'templates/header' );
    }
  ?>

<?php if(is_front_page() ): ?>

<div class="container">
    <div class="row">
        <section>
        <div class="wizard">
            <div class="wizard-inner">
                <div class="connecting-line"></div>
                <ul class="nav nav-tabs" role="tablist">

                    <li role="presentation" class="active">
                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-folder-open"></i>
                            </span>
                        </a>
                    </li>

                    <li role="presentation" class="disabled">
                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-pencil"></i>
                            </span>
                        </a>
                    </li>
                    <li role="presentation" class="disabled">
                        <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Step 3">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-picture"></i>
                            </span>
                        </a>
                    </li>

                    <li role="presentation" class="disabled">
                        <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-ok"></i>
                            </span>
                        </a>
                    </li>
                </ul>
            </div>

            <form role="form" method="post" id="solarForm">
                <div class="tab-content">
                    <div class="tab-pane active" role="tabpanel" id="step1">
                        <h3>Step 1</h3>
                         <h1>Test</h1>
                         <h1>Testing 2</h1>
                        <div class="form-group">
                            <h4>What is your annual electric bill?</h4>
                            <p>Why do we ask for your annual bill and not your monthly? Since monthly consumption can vary from month to month, or season to season, your annual bill is much better at predicting your energy needs year round.</p>
                            <select class="form-control" name="bill" id="bill">
                                <option value="1000">$1,000 - $1,500</option>
                                    <option value="1500">$1,500 - $2,000</option>
                                    <option value="2000">$2,000 - $2,500</option>
                                    <option value="2500">$2,500 - $3,000</option>
                                    <option value="3000">$3,000 - $3,500</option>
                                    <option value="3500">$3,500 - $4,000</option>
                                    <option value="4000">$4,000 - $4,500</option>
                                    <option value="4500">$4,500 - $5,000</option>
                                    <option value="5000">$5,000 - $5,500</option>
                                    <option value="5500">Above $5,500</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <h4>Who is your utility company?</h4>
                            <select class="form-control" name="company">
                                <option value="Edison">Edison (SCE)</option>
                                <option value="San Diego Gas &amp; Electric">San Diego Gas &amp; Electric (SDG&amp;E)</option>
                                <option value="Los Angeles Water and Power">Los Angeles Water and Power (LADWP)</option>
                                <option value="Riverside Utilities">Riverside Utilities (RPU)</option>
                                <option value="Imperial Irrigation District">Imperial Irrigation District (IID)</option>
                                <option value="Anaheim Water and Power">Anaheim Water and Power</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <h4>How much of your bill would you like to offset?*</h4>
                            <p>For example, if you want your solar system to completely zero out your annual electricity costs, choose 100%. If you want to add a buffer because you expect to consume more than your current consumption, select a value greater than 100%.</p>
                            <select class="form-control" name="offset" id ="offset">
                                <option value=".7" disabled>70%</option>
                                <option value=".8" disabled>80%</option>
                                <option value=".9" disabled>90%</option>
                                <option value="1">100%</option>
                                <option value="1.1">110%</option>
                                <option value="1.2">120%</option>
                                <option value="1.3">130%</option>
                                <option value="1.4">140%</option>
                                <option value="1.5">150%</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <h4>Type of Roof</h4>
                            <select class="form-control" name="roof" id ="roof">
                                <option value="">Composite Shingles</option>
                                <option value="">Flat Top</option>
                                <option value="">S Tile</option>
                                <option value="">Clay Flat</option>
                                <option value="">Ground Mount</option>
                                <option value="">I'm not sure what roof type I have</option>
                            </select>
                        </div>
                        <ul class="list-inline pull-right">
                            <li><button type="button" class="btn btn-primary next-step">Next</button></li>
                        </ul>
                    </div>
                    <div class="tab-pane" role="tabpanel" id="step2">
                        <h3>Step 2</h3>
                        <div class="kw-container">
                            <img src="/wp-content/uploads/2017/03/lightning.png" alt="" width="28" class="alignnone wp-image-125">
                            <div class="kilowatt orange bold"></div>
                            <p>Kilowatt Production</p>

                            <div class="included">
                                <h5>What's Included:</h5>
                                <ul>
                                    <li><span class="panelNumber"></span> Solar modules (25-year manufacturer performance guarantee)</li>
                                    <li>Inverter with Optimizers (10-year manufacturer warranty)</li>
                                    <li>System performance monitoring</li>
                                    <li>Balance of System</li>
                                    <li>Design & Engineering</li>
                                    <li>Complete installation (10 Year installation warranty)</li>
                                    <li>Utility approval processing</li>
                                </ul>
                            </div>
                            <div class="saving-wrap"> 
                                <h4 class="orange">This System Saves You:</h4>
                                <h4>$<span class="annual-savings"></span>/yr or $<span class="twentyfive"></span> Over 25 Years!</h4> 
                            </div>
                        </div>

                        <ul class="list-inline pull-right">
                            <li><button type="button" class="btn btn-default prev-step">Previous</button></li>
                            <li><button type="button" class="btn btn-primary next-step">Next</button></li>
                        </ul>
                    </div>
                    <div class="tab-pane" role="tabpanel" id="step3">
                        <h3>Step 3</h3>
                        <h5 class="orange bold">Net System Cost: <span class="net-cost">$</span></h5>
                        <h5>Monthly Payment: <span class="monthly-payment"></span></h5>
                        <h5>Tax Credit: <span class="tax-credit"></span></h5>
                        <div class="form-group">
                            <input type="tel" class="form-control" name="telephone" id="telephone" placeholder="951-555-5555" required>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" name="email" id="email" placeholder="Email" required>
                        </div>
                        <div class="kw-container">
                            <img src="/wp-content/uploads/2017/03/lightning.png" alt="" width="28" class="alignnone wp-image-125">
                            <div class="kilowatt orange bold"></div>
                            <p>Kilowatt Production</p>
                            <h5 class="orange bold">Net System Cost: <span class="net-cost">$</span></h5>

                            <div class="included">
                                <h5>What's Included:</h5>
                                <ul>
                                    <li><span class="panelNumber"></span> Solar modules (25-year manufacturer performance guarantee)</li>
                                    <li>Inverter with Optimizers (10-year manufacturer warranty)</li>
                                    <li>System performance monitoring</li>
                                    <li>Balance of System</li>
                                    <li>Design & Engineering</li>
                                    <li>Complete installation (10 Year installation warranty)</li>
                                    <li>Utility approval processing</li>
                                </ul>
                            </div>
                            <div class="saving-wrap"> 
                                <h4 class="orange">This System Saves You:</h4>
                                <h4>$<span class="annual-savings"></span>/yr or $<span class="twentyfive"></span> Over 25 Years!</h4> 
                            </div>
                        </div>
                        <h5 class="orange bold">Net System Cost: <span class="net-cost">$</span></h5>
                        <ul class="list-inline pull-right">
                            <li><button type="button" class="btn btn-default prev-step">Previous</button></li>
                            <li><button type="button" class="btn btn-primary btn-info-full next-step">Next</button></li>
                        </ul>
                    </div>
                    <div class="tab-pane" role="tabpanel" id="complete">
                        <h3>Email has been sent here</h3>
                        <p>You have successfully completed all steps.</p>
                        <input type="submit" value="Submit" id="sign_up">
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
    </section>
   </div>
</div>



<?php endif; ?>

<script src="<?php echo get_template_directory_uri() . '/assets/js/jquery.validate.min.js'; ?>"></script>

<?php /*
<script type="text/javascript">
$(document).ready(function(){
    console.log('Send Email!');
    $('#sign_up').click(function(){

    // send email to client before moving onto worldpay payment form
    var data = {
        action: 'mail_before_submit',
        toemail: '<?php echo $email ?>', // change this to the email field on your form
        _ajax_nonce: $('#my_email_ajax_nonce').data('nonce'),
    };
    jQuery.post(window.location.origin + "/wp-admin/admin-ajax.php", data, function(response) {
        console.log('Got this from the server: ' + response);
    });

    }); 
});
</script>
*/ ?>




<script type="text/javascript">
$(document).ready(function () {
    var form = $("#solarForm");

    //Removes the disabled if anything over $1000 is selected
    //Also sets 70% as default
    $('#bill').on('change', function (e)  {
        if ($(this).val() !=1000) {
            console.log('Disabled!')
            $("#offset option[value='.7']").attr("disabled", false);
            $("#offset option[value='.7']").prop("selected", "selected");
            $("#offset option[value='.8']").attr("disabled", false);
            $("#offset option[value='.9']").attr("disabled", false);
            return false;
        }
    });

    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
        e.target // newly activated tab
        e.relatedTarget // previous active tab
        console.log(e.target.hash);
        //console.log(e.relatedTarget);
        if (e.target.hash == '#step2') {
            //Get
            var bill = $('#bill').val();
            var offset = $('#offset').val();

            //Kilowatt Product 
            var kiloWatt = (bill / .23) * offset;

            //Panel Rate
            var panels = kiloWatt / 456;
            var panelRound = Math.round(panels);

            //Convert panel into price and killowat 
            var production = panels * 305;
            var convert = production * .001;
            var price = convert * 3250; 

            //Annual Savings
            var saving = bill * offset; 

            //25 Year Saving
            var yearlySavings = saving * 2 * 25;

            //Monthly Payment
            var monthlyPayment = price * .006873;

            //Tax Credit
            var taxCredit = price * .3;           

            console.log(bill);
            console.log(offset);
            console.log(kiloWatt);
            console.log(panels);
            console.log(convert);
            console.log(price);
            console.log(saving);
            console.log(yearlySavings);
            console.log(monthlyPayment);
            console.log(taxCredit);






            //Display Killowat on Screen 
            $(".kilowatt").text(convert.toFixed(2)); 

            //Display price with $12900 being the lowest
            if (price < 12900) {
                $(".net-cost").text('$12,900');
            } else {
                $(".net-cost").text('$' + price.toFixed(2));
            }

            //Display amount of Panels needed
            $(".panelNumber").text(panelRound); 

            //Display yearly saving
            $(".annual-savings").text(saving);
            //Display 25 year saving
            $(".twentyfive").text(yearlySavings);  

            //Display monthly payment
            $(".monthly-payment").text('$' + monthlyPayment.toFixed(2));  

            //Eligible Tax Credit
            $(".tax-credit").text('$' + taxCredit.toFixed(2));

        }


        if (e.target.hash == '#complete') {
            console.log("HI");
            console.log(e.target.hash);
            
            form.validate({
                rules: {
                    email: {
                        required: true,
                        minlength: 6,
                    },
                    
                },
                messages: {
                    email: {
                        required: "Username required",
                    },
                    
                }
            });
            if (form.valid() == true){
                console.log('If statement!');
            } else {
                console.log('Else statement!');
                $(e.target).tab('hide');
            }        
        }
    })

    /*
    $( "#step3 .next-step" ).click(function() {
        console.log('Testing');
        var form = $("#solarForm");
        form.validate({
            rules: {
                email: {
                    required: true,
                    minlength: 6,
                },
                
            },
            messages: {
                email: {
                    required: "Username required",
                },
                
            }
        });
        if (form.valid() == true){
            console.log('I fstatement!');
        } else {
            console.log('Else statement!');
        }        
    });
    */


    //Initialize tooltips
    $('.nav-tabs > li a[title]').tooltip();
    
    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);
    
        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $(".next-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);

    });
    $(".prev-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        prevTab($active);

    });
});

function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}    
</script>





  <div class="site-main wrap container" role="document">
    <div class="content row">
      <main class="main <?php echo roots_main_class(); ?>" role="main">
        <?php include roots_template_path(); ?>
      </main><!-- /.main -->
      <?php if (roots_display_sidebar()) : ?>
        <aside class="sidebar <?php echo roots_sidebar_class(); ?>" role="complementary">
          <?php include roots_sidebar_path(); ?>
        </aside><!-- /.sidebar -->
      <?php endif; ?>
    </div><!-- /.content -->
  </div><!-- /.wrap -->


  <?php get_template_part('templates/footer'); ?>

</body>
</html>
