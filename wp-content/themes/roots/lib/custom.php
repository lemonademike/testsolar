<?php
/**
 * Custom functions
 *
 * More for misc. functions, not a catch all
 *
 * Use:
 * image-sizes.php - to manage image sizes
 * widgets.php     - to manage widget areas
 */

add_action( 'phpmailer_init', 'bish_configMH', 10, 1 );
function bish_configMH( $phpmailer ) {
	// Define that we are sending with SMTP
	$phpmailer->isSMTP();
	// The hostname of the mailserver
	$phpmailer->Host = 'localhost';
	// Use SMTP authentication (true|false)
	$phpmailer->SMTPAuth = false;
	// SMTP port number
	// Mailhog normally run on port 1025
	$phpmailer->Port = WP_DEBUG ? '1025' : '25';
	// Username to use for SMTP authentication
	// $phpmailer->Username = 'yourusername';
	// Password to use for SMTP authentication
	// $phpmailer->Password = 'yourpassword';
	// The encryption system to use - ssl (deprecated) or tls
	// $phpmailer->SMTPSecure = 'tls';
	$phpmailer->From = 'site_adm@wp.local';
	$phpmailer->FromName = 'WP DEV';
}



// if you want only logged in users to access this function use this hook
add_action('wp_ajax_mail_before_submit', 'mycustomtheme_send_mail_before_submit');

// if you want none logged in users to access this function use this hook
add_action('wp_ajax_nopriv_mail_before_submit', 'mycustomtheme_send_mail_before_submit');

// if you want both logged in and anonymous users to get the emails, use both hooks above

function mycustomtheme_send_mail_before_submit(){
    check_ajax_referer('my_email_ajax_nonce');
    if ( isset($_POST['action']) && $_POST['action'] == "mail_before_submit" ){

    //send email  wp_mail( $to, $subject, $message, $headers, $attachments ); ex:
        wp_mail('<?php echo $email; ?>','this is the email subject line','email message body');
        echo 'email sent';
        die();
    }
    echo 'error';
    die();
}